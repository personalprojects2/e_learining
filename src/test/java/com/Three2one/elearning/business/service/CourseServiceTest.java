package com.Three2one.elearning.business.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.entity.Course;
import com.Three2one.e_learning.business.mapper.CourseMapper;
import com.Three2one.e_learning.business.repo.CourseRepositery;
import com.Three2one.e_learning.business.service.CourseService;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceTest {

	@InjectMocks
	private CourseService courseService;

	@Mock
	private CourseRepositery courseRepositery;

	@Mock
	private CourseMapper courseMapper;

	@Test
	public void test_getCourseByName() {
		Mockito.when(courseRepositery.findByName("Java")).thenReturn(getCourses());
		Course course = courseService.getCourseByName("Java");
	}

	@Test
	public void test_getCourseById() {
		Mockito.when(courseRepositery.findById(1L)).thenReturn(getOptionalCourse());
		Course course = courseService.getCourseById(1L);
	}

	@Test
	public void test_saveCourseDTO() {
		Mockito.when(courseMapper.getCourseEntityFromDTO(getCourseDto())).thenReturn(getJavaCourse());
		courseService.saveCourseDTO(getCourseDto());
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		courses.add(getJavaCourse());
		return courses;
	}

	private Course getJavaCourse() {
		Course course = new Course();
		course.setId(1L);
		course.setInstructor("Mark");
		course.setName("Java Course");
		return course;
	}

	private List<CourseDTO> getCourseDtoList() {
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs.add(getCourseDto());
		return courseDTOs;
	}

	private CourseDTO getCourseDto() {
		CourseDTO courseDTO = new CourseDTO();
		courseDTO.setId(1L);
		courseDTO.setName("Java Course");
		return courseDTO;
	}

	private Optional<Course> getOptionalCourse() {
		return Optional.of(getJavaCourse());
	}

}
