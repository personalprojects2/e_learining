package com.Three2one.elearning.business.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.dto.StudentDTO;
import com.Three2one.e_learning.business.entity.Course;
import com.Three2one.e_learning.business.entity.Student;
import com.Three2one.e_learning.business.exceptions.BadRequestException;
import com.Three2one.e_learning.business.mapper.CourseMapper;
import com.Three2one.e_learning.business.mapper.StudentMapper;
import com.Three2one.e_learning.business.repo.StudentRepositery;
import com.Three2one.e_learning.business.service.CourseService;
import com.Three2one.e_learning.business.service.StudentService;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

	@InjectMocks
	private StudentService studentService;

	@Mock
	private StudentRepositery studentRepositery;

	@Mock
	private CourseService courseService;

	@Mock
	private StudentMapper studentMapper;

	@Mock
	private CourseMapper courseMapper;

	@Test
	public void test_getStudentByName() {
		Mockito.when(studentRepositery.findByName("Mahmoud")).thenReturn(getStudentList());
		Student student = studentService.getStudentByName("Mahmoud");
		assertNotNull(student);
		assertEquals("Mahmoud", student.getName());
	}

	@Test
	public void test_getStudentById() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(getOptionalStudent());
		Student student = studentService.getStudentById(1L);
		assertNotNull(student);
		assertEquals("Mahmoud", student.getName());
	}

	@Test
	public void test_saveStudentEntity() {
		Mockito.when(studentRepositery.save(getStudent())).thenReturn(getStudent());
		Student student = studentService.saveStudentEntity(getStudent());
		assertNotNull(student);
		assertEquals("Mahmoud", student.getName());
	}

	@Test
	public void test_getStudentDTOById() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(getOptionalStudent());
		Mockito.when(studentMapper.getStudentDTOFromEntity(getStudent())).thenReturn(getStudentDto());
		StudentDTO studentDTO = studentService.getStudentDTOById(1L);
		assertNotNull(studentDTO);
		assertEquals("Mahmoud", studentDTO.getName());
	}

	@Test
	public void test_getAllStudentsDTOS() {
		Mockito.when(studentRepositery.findAll()).thenReturn(getStudentList());
		Mockito.when(studentMapper.getStudentDTOsFromEntities(getStudentList())).thenReturn(getStudentDtoList());
		List<StudentDTO> studentDTOs = studentService.getAllStudentsDTOS();
		assertNotNull(studentDTOs);
		assertEquals(1, studentDTOs.size());
	}

	@Test
	public void test_saveStudentDTO() {
		Mockito.when(studentMapper.getStudentEntityFromDTO(getStudentDto())).thenReturn(getStudent());
		Mockito.when(studentRepositery.save(getStudent())).thenReturn(getStudent());
		Mockito.when(studentMapper.getStudentDTOFromEntity(getStudent())).thenReturn(getStudentDto());
		StudentDTO studentDTO = studentService.saveStudentDTO(getStudentDto());
		assertNotNull(studentDTO);
		assertEquals(1, studentDTO.getId());
	}

	@Test(expected = BadRequestException.class)
	public void test_saveStudentDTO_nullvalue() {
		studentService.saveStudentDTO(null);
	}

	
	@Test(expected = BadRequestException.class)
	public void test_getRegisteredCoursesForStudent_nullStudentEntity() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(null);
		studentService.getRegisteredCoursesForStudent(1L);
	}

	@Test(expected = BadRequestException.class)
	public void test_registerStudentToCourse_nullStudent() {
		studentService.registerStudentToCourse(1L, 1L);
	}

	@Test(expected = BadRequestException.class)
	public void test_registerStudentToCourse_nullCourse() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(getOptionalStudent());
		studentService.registerStudentToCourse(1L, 1L);
	}

	@Test
	public void test_registerStudentToCourse() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(getOptionalStudent());
		Mockito.when(courseService.getCourseById(1L)).thenReturn(getJavaCourse());
		Student student = studentService.registerStudentToCourse(1L, 1L);
	}
	
	@Test
	public void test_registerStudentToCourse_NoCourses() {
		Mockito.when(studentRepositery.findById(1L)).thenReturn(getOptionalStudent());
		Mockito.when(courseService.getCourseById(1L)).thenReturn(getJavaCourse());
		Student student = studentService.registerStudentToCourse(1L, 1L);
	}
	/////////////

	private Student getStudent() {
		Student student = new Student();
		student.setId(1L);
		student.setName("Mahmoud");
		student.setUserName("mahmoudmatar");
		student.setEmail("mahmoudmatar@gmail.com");
		student.setCourses(null);
		return student;
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		courses.add(getJavaCourse());
		return courses;
	}

	private Course getJavaCourse() {
		Course course = new Course();
		course.setId(1L);
		course.setInstructor("Mark");
		course.setName("Java Course");
		return course;
	}

	private List<CourseDTO> getCourseDtoList() {
		List<CourseDTO> courseDTOs = new ArrayList<>();
		courseDTOs.add(getCourseDto());
		return courseDTOs;
	}

	private CourseDTO getCourseDto() {
		CourseDTO courseDTO = new CourseDTO();
		courseDTO.setId(1L);
		courseDTO.setName("Java Course");
		return courseDTO;
	}

	private StudentDTO getStudentDto() {
		StudentDTO studentDTO = new StudentDTO();
		studentDTO.setId(1L);
		studentDTO.setName("Mahmoud");
		studentDTO.setUserName("mahmoudmatar");
		studentDTO.setEmail("mahmoudmatar@gmail.com");
		return studentDTO;
	}

	private List<Student> getStudentList() {
		List<Student> students = new ArrayList<>();
		students.add(getStudent());
		return students;
	}

	private List<StudentDTO> getStudentDtoList() {
		List<StudentDTO> studentDtos = new ArrayList<>();
		studentDtos.add(getStudentDto());
		return studentDtos;
	}

	private Optional<Student> getOptionalStudent() {
		return Optional.of(getStudent());
	}

	private Optional<Student> getOptionalStudentRegisteredCourses() {
		Student student = getStudent();
		student.setCourses(getCourses());
		return Optional.of(student);
	}
}
