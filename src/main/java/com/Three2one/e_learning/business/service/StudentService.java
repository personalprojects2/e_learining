package com.Three2one.e_learning.business.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.dto.StudentDTO;
import com.Three2one.e_learning.business.entity.Course;
import com.Three2one.e_learning.business.entity.Student;
import com.Three2one.e_learning.business.exceptions.BadRequestException;
import com.Three2one.e_learning.business.mapper.CourseMapper;
import com.Three2one.e_learning.business.mapper.StudentMapper;
import com.Three2one.e_learning.business.repo.StudentRepositery;

@Service
public class StudentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentService.class);

	@Autowired
	private StudentRepositery studentRepositery;

	@Autowired
	private CourseService courseService;

	@Autowired
	private StudentMapper studentMapper;

	@Autowired
	private CourseMapper courseMapper;

	/**
	 * Getting StudentEntity by name.
	 * 
	 * @param studentName
	 * @return StudentEntity
	 */
	public Student getStudentByName(String studentName) {
		List<Student> studentEntities = studentRepositery.findByName(studentName);
		if (studentEntities == null || studentEntities.isEmpty()) {
			return null;
		}
		return studentEntities.get(0);
	}

	/**
	 * getStudentById
	 * 
	 * @param id
	 * @return StudentEntity
	 */
	public Student getStudentById(Long id) {
		Optional<Student> optionalStudentEntity = studentRepositery.findById(id);
		if (optionalStudentEntity != null && optionalStudentEntity.isPresent()) {
			return optionalStudentEntity.get();
		}
		return null;
	}

	/**
	 * Save StudentEntity.
	 * 
	 * @param studentEntity
	 * @return saved studentEntity
	 */
	@Transactional
	public Student saveStudentEntity(Student studentEntity) {
		return studentRepositery.save(studentEntity);
	}

	public StudentDTO getStudentDTOById(Long id) {
		Student studentEntity = getStudentById(id);
		return studentMapper.getStudentDTOFromEntity(studentEntity);
	}
	
	public void deleteStudentDTOById(Long id) {
		Student studentEntity = getStudentById(id);
		studentRepositery.delete(studentEntity);
	}

	/**
	 * Getting all Students DTOS
	 * 
	 * @return
	 */
	public List<StudentDTO> getAllStudentsDTOS() {
		List<Student> studentEntities = (List<Student>) studentRepositery.findAll();
		return studentMapper.getStudentDTOsFromEntities(studentEntities);
	}

	/**
	 * Save or update StudentEntity.
	 * 
	 * @param studentDTO
	 * @return StudentDTO updated or saved.
	 */
	public StudentDTO saveStudentDTO(StudentDTO studentDTO) {
		LOGGER.info("Saving studentdto");
		if (studentDTO == null) {
			throw new BadRequestException("Student Model is not presented");
		}
		Student studentEntity = studentMapper.getStudentEntityFromDTO(studentDTO);
		// Save Entity
		Student savedStudentEntity = saveStudentEntity(studentEntity);
		return studentMapper.getStudentDTOFromEntity(savedStudentEntity);
	}

	/**
	 * getRegisteredCoursesForStudent
	 * 
	 * @param studentId
	 * @return List of Courses
	 */
	public List<CourseDTO> getRegisteredCoursesForStudent(Long studentId) {
		LOGGER.info("Getting Registered Courses for Student id : {} ", studentId);
		Student studentEntity = getStudentById(studentId);
		if (studentEntity == null) {
			LOGGER.error("Student with Id : {} not found", studentId);
			throw new BadRequestException("Student with Id{" + studentId + "} not found");
		}
		List<Course> courseEntities = studentEntity.getCourses();
		return courseMapper.getCourseDTOsFromEntities(courseEntities);
	}

	/**
	 * register Student To Course
	 * 
	 * @param studentId
	 * @param courseId
	 * @return Student Saved object
	 */
	public Student registerStudentToCourse(Long studentId, Long courseId) {
		// Getting Student
		LOGGER.info("Register Student id : {} with Course Id : {}", studentId, courseId);
		Student studentEntity = getStudentById(studentId);
		if (studentEntity == null) {
			LOGGER.error("Student with Id : {} not found", studentId);
			throw new BadRequestException("Student with Id{" + studentId + "} not found");
		}
		// Getting Course
		Course courseEntity = courseService.getCourseById(courseId);
		if (courseEntity == null) {
			LOGGER.error("Course with Id : {} not found", courseId);
			throw new BadRequestException("Course with Id{" + courseId + "} not found");
		}
		// Checking if Student is not registered before
		if (studentEntity.getCourses() != null && studentEntity.getCourses() != null
				&& studentEntity.getCourses().contains(courseEntity)) {
			LOGGER.error("Student with Id : {} already registered to course id : {} ", studentId, courseId);
			throw new BadRequestException(
					"Student with Id : {" + studentId + "} already registered to course id : {" + courseId + "}");
		}
		if (studentEntity.getCourses() != null) {
			studentEntity.getCourses().add(courseEntity);
		} else {
			List<Course> courses = new ArrayList<>();
			courses.add(courseEntity);
			studentEntity.setCourses(courses);
		}
		return saveStudentEntity(studentEntity);
	}

	public Student unRegisterStudentToCourse(Long studentId, Long courseId) {
		// Getting Student
		LOGGER.info("UnRegister Student id : {} from Course Id : {}", studentId, courseId);
		Student studentEntity = getStudentById(studentId);
		if (studentEntity == null) {
			LOGGER.error("Student with Id : {} not found", studentId);
			throw new BadRequestException("Student with Id{" + studentId + "} not found");
		}
		// Getting Course
		Course courseEntity = courseService.getCourseById(courseId);
		if (courseEntity == null) {
			LOGGER.error("Course with Id : {} not found", courseId);
			throw new BadRequestException("Course with Id{" + courseId + "} not found");
		}
		// Checking if Student is not registered before
		if (studentEntity.getCourses() != null && studentEntity.getCourses() != null
				&& !studentEntity.getCourses().contains(courseEntity)) {
			LOGGER.error("Student with Id : {} already not registered to course id : {} ", studentId, courseId);
			throw new BadRequestException(
					"Student with Id : {" + studentId + "} already not registered to course id : {" + courseId + "}");
		}
		if (studentEntity.getCourses() != null) {
			studentEntity.getCourses().remove(courseEntity);
		}
		return saveStudentEntity(studentEntity);
	}

}
