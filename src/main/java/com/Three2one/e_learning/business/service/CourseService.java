package com.Three2one.e_learning.business.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Three2one.e_learning.business.dto.CourseDTO;
import com.Three2one.e_learning.business.entity.Course;
import com.Three2one.e_learning.business.exceptions.BadRequestException;
import com.Three2one.e_learning.business.mapper.CourseMapper;
import com.Three2one.e_learning.business.repo.CourseRepositery;

@Service
public class CourseService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CourseService.class);

	@Autowired
	private CourseRepositery courseRepositery;

	@Autowired
	private CourseMapper courseMapper;

	/**
	 * Getting CourseEntity by name.
	 * 
	 * @param courseName
	 * @return CourseEntity
	 */
	public Course getCourseByName(String courseName) {
		List<Course> courseEntities = courseRepositery.findByName(courseName);
		if (courseEntities == null || courseEntities.isEmpty()) {
			return null;
		}
		return courseEntities.get(0);
	}

	/**
	 * getCourseById
	 * 
	 * @param id
	 * @return CourseEntity
	 */
	public Course getCourseById(Long id) {
		Optional<Course> optionalCourseEntity = courseRepositery.findById(id);
		if (optionalCourseEntity.isPresent()) {
			return optionalCourseEntity.get();
		}
		return null;
	}

	/**
	 * Save CourseEntity.
	 * 
	 * @param courseEntity
	 * @return saved courseEntity
	 */
	@Transactional
	public Course saveCourseEntity(Course courseEntity) {
		return courseRepositery.save(courseEntity);
	}

	public CourseDTO getCourseDTOById(Long id) {
		Course courseEntity = getCourseById(id);
		return courseMapper.getCourseDTOFromEntity(courseEntity);
	}
	
	public void deleteCourseDTOById(Long id) {
		Course courseEntity = getCourseById(id);
		courseRepositery.delete(courseEntity);
	}

	/**
	 * Getting all Courses DTOS
	 * 
	 * @return
	 */
	public List<CourseDTO> getAllCoursesDTOS() {
		List<Course> courseEntities = (List<Course>) courseRepositery.findAll();
		return courseMapper.getCourseDTOsFromEntities(courseEntities);
	}

	/**
	 * Save or update CourseEntity.
	 * 
	 * @param courseDTO
	 * @return CourseDTO updated or saved.
	 */
	public CourseDTO saveCourseDTO(CourseDTO courseDTO) {
		if (courseDTO == null) {
			throw new BadRequestException("Course Model is not presented");
		}
		LOGGER.info("Saving Course DTO with details : {} ", courseDTO);
		Course courseEntity = courseMapper.getCourseEntityFromDTO(courseDTO);
		if (courseEntity.getId() == null) {
			courseEntity.setPublishDate(new Date());
		}
		// Modify LastUpdated date in all cases.
		courseEntity.setLastUpdated(new Date());
		// Save Entity
		Course savedCourseEntity = saveCourseEntity(courseEntity);
		LOGGER.info("Course Saved Successfully");
		return courseMapper.getCourseDTOFromEntity(savedCourseEntity);
	}

}
