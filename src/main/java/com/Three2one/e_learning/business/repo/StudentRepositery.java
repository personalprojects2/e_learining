package com.Three2one.e_learning.business.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.Three2one.e_learning.business.entity.Student;

public interface StudentRepositery extends CrudRepository<Student, Long> {

	List<Student> findByName(String name);

}
