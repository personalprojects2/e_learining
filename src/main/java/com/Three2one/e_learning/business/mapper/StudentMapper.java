package com.Three2one.e_learning.business.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.Three2one.e_learning.business.dto.StudentDTO;
import com.Three2one.e_learning.business.entity.Student;

@Component
public class StudentMapper {

	public List<StudentDTO> getStudentDTOsFromEntities(List<Student> studentEntities) {
		List<StudentDTO> studentDTOs = new ArrayList<>();
		// If Passed List is empty
		if (studentEntities == null || studentEntities.isEmpty()) {
			return studentDTOs;
		}
		for (Student studentEntity : studentEntities) {
			StudentDTO studentDTO = getStudentDTOFromEntity(studentEntity);
			studentDTOs.add(studentDTO);
		}
		// return DTOS
		return studentDTOs;
	}

	public StudentDTO getStudentDTOFromEntity(Student studentEntity) {
		if (studentEntity == null) {
			return null;
		}
		StudentDTO studentDTO = new StudentDTO();
		studentDTO.setId(studentEntity.getId());
		studentDTO.setName(studentEntity.getName());
		studentDTO.setUserName(studentEntity.getUserName());
		studentDTO.setEmail(studentEntity.getEmail());
		studentDTO.setGender(studentEntity.getGender());
		studentDTO.setDateOfBirth(studentEntity.getDateOfBirth());
		studentDTO.setPassword(studentEntity.getPassword());
		// return StudentDto
		return studentDTO;
	}

	public Student getStudentEntityFromDTO(StudentDTO studentDTO) {
		if (studentDTO == null) {
			return null;
		}
		Student studentEntity = new Student();
		studentEntity.setId(studentDTO.getId());
		studentEntity.setName(studentDTO.getName());
		studentEntity.setUserName(studentDTO.getUserName());
		studentEntity.setEmail(studentDTO.getEmail());
		studentEntity.setGender(studentDTO.getGender());
		studentEntity.setDateOfBirth(studentDTO.getDateOfBirth());
		studentEntity.setPassword(studentDTO.getPassword());
		// return studentEntity
		return studentEntity;
	}

}
